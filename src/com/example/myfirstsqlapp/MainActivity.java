package com.example.myfirstsqlapp;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.myfirstsqlapp.TableContract.Entry;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	/**
	 * onClick-Method
	 *
	 * @param view
	 */
	public void showTable(View view) {
		updateTableView();
	}
	
	private void updateInfoLine(String info) {
		TextView editText = (TextView) findViewById(R.id.infoLine);
		
		editText.setText("Info: " + info);
	}
	
	/**
	 * Updates the UI with new or old data from Database.
	 */
	private void updateTableView() {
		TextView editText = (TextView) findViewById(R.id.tableContent);
		String tableContent = readTableContentFromDB();
		editText.setText(tableContent);
	}

	/**
	 * Prepare database access.
	 * 
	 * @return
	 */
	private TableContract.SQLHelper getDBHelper() {
		TableContract.SQLHelper mDbHelper = new TableContract.SQLHelper(this);
		
		return mDbHelper;
	}
	
	private String readTableContentFromDB() {
		SQLiteDatabase db = getDBHelper().getReadableDatabase();

		// Define a projection that specifies which columns from the database
		// you will actually use after this query.
		String[] projection = {
		    Entry._ID,
		    Entry.COLUMN_NAME_ENTRY_ID,
		    Entry.COLUMN_NAME_TEXT
		    };

		// How you want the results sorted in the resulting Cursor
		String sortOrder = Entry._ID + " ASC";
		
		// Define 'where' part of query.
		String selection = Entry._ID + " > ?"; // in this case equal to null
		// Specify arguments in placeholder order.
		String[] selectionArgs = { String.valueOf(0) };

		Cursor cursor = db.query(
		    Entry.TABLE_NAME,  // The table to query
		    projection,        // The columns to return
		    selection,         // The columns for the WHERE clause
		    selectionArgs,     // The values for the WHERE clause
		    null,              // don't group the rows
		    null,              // don't filter by row groups
		    sortOrder          // The sort order
		    );
		
		cursor.moveToFirst();
		
		StringBuffer buf = new StringBuffer();
		if (cursor.getCount() == 0) {
			// if curser.getCount() == 0 -> cursor.isAfterLast() is false
			return "Table is empty";
		}
		
		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			long id = cursor.getLong(cursor.getColumnIndexOrThrow(Entry._ID));
			String itemId = cursor.getString(cursor.getColumnIndexOrThrow(Entry.COLUMN_NAME_ENTRY_ID));
			String text = cursor.getString(cursor.getColumnIndexOrThrow(Entry.COLUMN_NAME_TEXT));
			
			buf.append(id).append(":");
			buf.append(itemId).append("-");
			buf.append(text).append("\n");
		}
		return buf.toString();
	}

	public void insertRow(View view) {
		EditText insertOne = (EditText) findViewById(R.id.insertOne);
		EditText insertTwo = (EditText) findViewById(R.id.insertTwo);
		
		String textOne = insertOne.getText().toString();
		String textTwo = insertTwo.getText().toString();
		
		// Gets the data repository in write mode
		SQLiteDatabase db = getDBHelper().getWritableDatabase();

		// Create a new map of values, where column names are the keys
		ContentValues values = new ContentValues();
		values.put(Entry.COLUMN_NAME_ENTRY_ID, textOne);
		values.put(Entry.COLUMN_NAME_TEXT, textTwo);

		// Insert the new row, returning the primary key value of the new row
		long inserted = db.insert(
		         Entry.TABLE_NAME,
		         Entry.COLUMN_NAME_NULLABLE,
		         values);
		
		updateInfoLine(inserted + " rows inserted");
		updateTableView();
	}
	

	public void updateRow(View view) {
		EditText idText = (EditText) findViewById(R.id.updateId);
		int id = Integer.parseInt(idText.getText().toString());
		EditText oneText = (EditText) findViewById(R.id.updateOne);
		EditText twoText = (EditText) findViewById(R.id.updateTwo);
		String valueOne = oneText.getText().toString();
		String valueTwo = twoText.getText().toString();
		
		SQLiteDatabase db = getDBHelper().getReadableDatabase();

		// New value for one column
		ContentValues values = new ContentValues();
		values.put(Entry.COLUMN_NAME_ENTRY_ID, valueOne);
		values.put(Entry.COLUMN_NAME_TEXT, valueTwo);

		// Which row to update, based on the ID
		String selection = Entry._ID + " = ?";
		String[] selectionArgs = { String.valueOf(id) };

		int updated = db.update(
		    TableContract.Entry.TABLE_NAME,
		    values,
		    selection,
		    selectionArgs);

		updateInfoLine(updated + " rows updated");
		updateTableView();
	}
	

	public void deleteRow(View view) {
		EditText idText = (EditText) findViewById(R.id.deleteId);
		int id = Integer.parseInt(idText.getText().toString());
		
		// Define 'where' part of query.
		String selection = Entry._ID + " = ?";
		// Specify arguments in placeholder order.
		String[] selectionArgs = { String.valueOf(id) };
		// Issue SQL statement.
		SQLiteDatabase db = getDBHelper().getWritableDatabase();
		int deleted = db.delete(Entry.TABLE_NAME, selection, selectionArgs);
		
		updateInfoLine(deleted + " rows deleted");
		updateTableView();
	}
}
